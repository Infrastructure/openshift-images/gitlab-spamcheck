FROM registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck/model:1.0.0 AS model

FROM registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck:latest
COPY --from=model /data /var/lib/spamcheck
